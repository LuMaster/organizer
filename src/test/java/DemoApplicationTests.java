import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import organizer.OrganizerApplication;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(classes = OrganizerApplication.class)
class DemoApplicationTests {

    @Autowired
    private OrganizerApplication application;

    @Test
    void contextLoads() {
        assertThat(application).isNotNull();
    }

}
