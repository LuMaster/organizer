package oragnizer.controllers;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import organizer.OrganizerApplication;
import organizer.model.Note;
import organizer.services.NoteRepository;

import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.List;

import static org.assertj.core.internal.bytebuddy.matcher.ElementMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = OrganizerApplication.class)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application.properties")
public class NoteControllerTest
{
    @Autowired
    private MockMvc mvc;

    @MockBean
    private NoteRepository noteRepository;

    @WithMockUser(username = "admin", password = "admin")
    @Test
    public void getAllNotesTest()
            throws Exception
    {

        Note note = new Note();
        note.setId(1);
        List<Note> allEmployees = Collections.singletonList(note);

        given(noteRepository.findAll()).willReturn(allEmployees);

        mvc.perform(get("/notes").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id", is(note.getId())).exists());
    }

    @WithMockUser(username = "admin", password = "admin")
    @Test
    public void deleteNoteTest()
            throws Exception
    {

        mvc.perform(delete("/notes/1")).andExpect(status().isOk());
    }

    @WithMockUser(username = "admin", password = "admin")
    @Test
    public void createNoteTest()
            throws Exception
    {
        Note value = new Note();
        value.setTitle("Tytuł");
        given(noteRepository.save(any())).willReturn(value);

        mvc.perform(post("/notes").requestAttr("city", "Cracow")
                            .requestAttr("country", "PL")
                            .requestAttr("title", "Tytuł")
                            .requestAttr("date", GregorianCalendar.from(ZonedDateTime.now()))
                            .requestAttr("entry", "W tym dniu skończyłem"))
                .andExpect(status().isOk());
    }

}
