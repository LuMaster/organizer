package oragnizer.model;

import org.junit.jupiter.api.Test;
import organizer.model.Note;

import java.time.ZonedDateTime;
import java.util.GregorianCalendar;

import static org.junit.Assert.assertTrue;

public class modelTest
{

    @Test
    public void testEquals()
    {
        ZonedDateTime now = ZonedDateTime.now();

        Note note = new Note();
        note.setId(1);
        note.setUserId(1);
        note.setTitle("Title");
        note.setEntry("Entry");
        note.setDate(GregorianCalendar.from(now).getTime());

        Note note2 = new Note();
        note2.setId(1);
        note2.setUserId(1);
        note2.setTitle("Title");
        note2.setEntry("Entry");
        note2.setDate(GregorianCalendar.from(now).getTime());

        assertTrue(note.equals(note2));
    }

}
