package organizer.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "Weather")
public class Weather
{

    @Id
    private int id;

    @NotNull
    private String countryCode;

    @NotNull
    private String cityName;

    @NotNull
    private String temp;

    @OneToOne()
    @MapsId
    private Note note;

    public Weather()
    {
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public void setCountryCode(String countryCode)
    {
        this.countryCode = countryCode;
    }

    public void setCityName(String cityName)
    {
        this.cityName = cityName;
    }

    public void setTemp(String temp)
    {
        this.temp = temp;
    }

    public void setNote(Note note)
    {
        this.note = note;
    }
}
