package organizer.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

import static javax.persistence.TemporalType.DATE;

@Entity
@Table(name = "Notes")
public class Note
{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @NotBlank
    @NotNull
    private String title;

    @NotNull
    @Temporal(DATE)
    private Date date;

    @NotBlank
    @NotNull
    private String entry;

    @NotNull
    private int userId;

    @OneToOne(cascade = CascadeType.ALL)
    private Weather weather;

    public Note()
    {
    }

    public Note(String title, Date date, String entry, int userId)
    {
        this.title = title;
        this.date = date;
        this.entry = entry;
        this.userId = userId;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public Date getDate()
    {
        return date;
    }

    public void setDate(Date date)
    {
        this.date = date;
    }

    public String getEntry()
    {
        return entry;
    }

    public void setEntry(String entry)
    {
        this.entry = entry;
    }

    public int getUserId()
    {
        return userId;
    }

    public void setUserId(int userId)
    {
        this.userId = userId;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Note)) {
            return false;
        }

        Note note = (Note)o;

        if (id != note.id) {
            return false;
        }
        if (userId != note.userId) {
            return false;
        }
        if (!title.equals(note.title)) {
            return false;
        }
        if (!date.equals(note.date)) {
            return false;
        }
        return entry.equals(note.entry);
    }

    public void setWeather(Weather weather)
    {
        weather.setNote(this);
        this.weather = weather;
    }

    public Weather getWeather()
    {
        return weather;
    }
}
