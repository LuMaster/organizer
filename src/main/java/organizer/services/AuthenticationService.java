package organizer.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import organizer.model.User;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class AuthenticationService
{

    private final UserRepository userRepository;

    private final AuthenticationManager authenticationManager;

    private static final Logger logger = LoggerFactory.getLogger(AuthenticationService.class);

    public AuthenticationService(UserRepository userRepository, AuthenticationManager authenticationManager)
    {
        this.userRepository = userRepository;
        this.authenticationManager = authenticationManager;
    }

    public boolean autoLogin(String username, String password)
    {
        Optional<User> firstByUsername = userRepository.findFirstByUsername(username);
        Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
        if (!firstByUsername.isPresent()) {
            return false;
        }
        User user = firstByUsername.get();
        if (!user.getPassword().equals(password)) {
            return false;
        }
        grantedAuthorities.add(new SimpleGrantedAuthority(user.getRole()));
        org.springframework.security.core.userdetails.User authenticatedUser = new org.springframework.security.core.userdetails.User(user.getUsername(),
                                                                                                                                      user.getPassword(),
                                                                                                                                      grantedAuthorities);
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(authenticatedUser,
                                                                                                                          password,
                                                                                                                          authenticatedUser.getAuthorities());
        authenticationManager.authenticate(usernamePasswordAuthenticationToken);
        if (usernamePasswordAuthenticationToken.isAuthenticated()) {
            SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
            logger.debug(String.format("Auto login %s successfully!", username));
        } else {
            logger.debug(String.format("Auto login %s unsuccessfully!", username));
        }
        return true;
    }

}
