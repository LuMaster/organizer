package organizer.services;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import organizer.model.User;

import java.util.Optional;

@Component
public class UserDataServiceImpl implements UserDetailsService {

    public final UserRepository userRepository;

    public UserDataServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) {
        Optional<User> firstByUsername = userRepository.findFirstByUsername(username);
        if (!firstByUsername.isPresent()) {
            throw new UsernameNotFoundException("User not found.");
        }
        User user = firstByUsername.get();
        org.springframework.security.core.userdetails.User.UserBuilder builder = org.springframework.security.core.userdetails.User.withUsername(username);
        String encode = new BCryptPasswordEncoder().encode(user.getPassword());
        builder.password(encode);
        builder.roles(user.getRole());
        return builder.build();
    }


}
