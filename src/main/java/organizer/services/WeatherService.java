package organizer.services;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import organizer.model.Weather;

import java.io.IOException;

@Service
public class WeatherService
{
    private static final Logger logger = LoggerFactory.getLogger(WeatherService.class);

    @Value("${weatherApi.key}")
    public String weatherApi;

    public Weather getWeatherForCity(String city, String country)
    {
        OkHttpClient client = new OkHttpClient();

        String format = String.format("https://api.weatherbit.io/v2.0/current?city=%s&country=%s&key=%s", city, country, weatherApi);

        Request request = new Request.Builder().url(format).get().build();
        try {
            Response response = client.newCall(request).execute();
            String json = response.body().string();
            final ObjectNode node = new ObjectMapper().readValue(json, ObjectNode.class);
            JsonNode countryCode = node.findValue("country_code");
            JsonNode cityName = node.findValue("city_name");
            JsonNode temp = node.findValue("temp");
            if (countryCode != null && cityName != null && temp != null) {
                Weather weather = new Weather();
                weather.setCityName(cityName.textValue());
                weather.setCountryCode(countryCode.textValue());
                weather.setTemp(temp.asText());
                return weather;
            }
        }
        catch (IOException e) {
            logger.error("Could not find weather!",e);
        }
        return null;
    }

}
