package organizer.services;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;
import organizer.model.Note;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
public interface NoteRepository extends JpaRepository<Note, Integer> {

    Optional<Note> findById(Integer noteId);

    List<Note> findAllByUserId(Integer id);

    @Transactional
    @Modifying
    void deleteByUserId(Integer userId);
}
