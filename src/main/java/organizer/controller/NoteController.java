package organizer.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import organizer.model.Note;
import organizer.model.User;
import organizer.model.Weather;
import organizer.services.NoteRepository;
import organizer.services.UserRepository;
import organizer.services.WeatherService;

import javax.validation.Valid;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

@RestController
public class NoteController
{

    private final NoteRepository noteRepository;

    private final UserRepository userRepository;

    private final WeatherService weatherService;

    @Autowired
    public NoteController(NoteRepository noteRepository, UserRepository userRepository, WeatherService weatherService)
    {
        this.weatherService = weatherService;
        Assert.notNull(noteRepository, "NoteService cannot be null!");
        Assert.notNull(userRepository, "UserRepository cannot be null!");
        this.noteRepository = noteRepository;
        this.userRepository = userRepository;
    }

    @ResponseBody
    @GetMapping("/notes/{nodeId}")
    public Note findByNoteId(@PathVariable(value = "nodeId") int id)
    {
        return noteRepository.findById(id).orElse(null);
    }

    @ResponseBody
    @PostMapping(value = "/notes")
    public ResponseEntity<Note> createNote(@RequestAttribute("city") @Valid String city, @RequestAttribute("country") @Valid String country,
            @RequestAttribute("title") @Valid String title, @RequestAttribute("date") @Valid Date date,
            @RequestAttribute("entry") @Valid String entry)
    {
        Note note = new Note();
        note.setTitle(title);
        note.setDate(date);
        note.setEntry(entry);
        Weather weatherForCity = weatherService.getWeatherForCity(city, country);
        note.setWeather(weatherForCity);

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Object principal = authentication.getPrincipal();
        if (principal instanceof UserDetails) {
            String username = ((UserDetails)principal).getUsername();
            Optional<User> firstByUsername = userRepository.findFirstByUsername(username);
            if (firstByUsername.isPresent()) {
                note.setUserId(firstByUsername.get().getId());
                Note save = noteRepository.save(note);
                return ResponseEntity.status(HttpStatus.OK).body(save);
            } else {
                ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
            }
        }
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
    }

    @ResponseBody
    @GetMapping("/notes")
    public List<Note> findAllNotes()
    {
        Iterator<Note> all = noteRepository.findAll().iterator();
        Stream<Note> noteStream = StreamSupport.stream(Spliterators.spliteratorUnknownSize(all, Spliterator.ORDERED), false);
        return noteStream.collect(Collectors.toList());
    }

    @ResponseStatus(value = HttpStatus.OK)
    @DeleteMapping("/notes/{id}")
    public void deleteNote(@PathVariable int id)
    {
        noteRepository.deleteById(id);
    }
}
