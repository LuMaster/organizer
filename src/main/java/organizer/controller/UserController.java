package organizer.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import organizer.model.Note;
import organizer.model.User;
import organizer.services.AuthenticationService;
import organizer.services.NoteRepository;
import organizer.services.UserRepository;

import javax.validation.Valid;
import java.util.List;

@RestController
public class UserController
{

    private final UserRepository userRepository;

    private final NoteRepository noteRepository;

    private final AuthenticationService authenticationService;

    @Autowired
    public UserController(UserRepository userRepository, NoteRepository noteRepository, AuthenticationService authenticationService)
    {
        Assert.notNull(userRepository, "UserRepository cannot be null!");
        Assert.notNull(noteRepository, "NoteRepository cannot be null!");
        Assert.notNull(authenticationService, "AuthenticationService cannot be null!");
        this.userRepository = userRepository;
        this.noteRepository = noteRepository;
        this.authenticationService = authenticationService;
    }

    @ResponseBody
    @PostMapping(value = "/users/login", consumes = { "multipart/form-data" })
    public ResponseEntity<String> create(@RequestPart("username") @Valid String username, @RequestPart("password") @Valid String password)
    {
        boolean autoLogin = authenticationService.autoLogin(username, password);
        if (autoLogin) {
            return new ResponseEntity<>("Logged successfully", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Authentication Fail", HttpStatus.UNAUTHORIZED);
        }
    }

    @ResponseBody
    @PostMapping(value = "/users/create")
    public User create(@RequestAttribute("username") @Valid String username, @RequestAttribute("password") @Valid String password,
            @RequestAttribute("email") @Valid String email)
    {
        User user = new User();
        user.setUsername(username);
        user.setPassword(password);
        user.setEmail(email);
        if (username.toLowerCase().contains("admin")) {
            user.setRole("ADMIN");
        } else {
            user.setRole("USER");
        }
        return userRepository.save(user);
    }

    @ResponseBody
    @GetMapping(value = "/users")
    public List<User> findAll()
    {
        return userRepository.findAll();
    }

    @ResponseBody
    @GetMapping(value = "/users/{id}")
    public User findAll(@PathVariable int id)
    {
        return userRepository.findById(id).orElse(null);
    }

    @ResponseStatus(value = HttpStatus.OK)
    @DeleteMapping(value = "/users/{id}")
    public void delete(@PathVariable int id)
    {
        userRepository.deleteById(id);
    }

    @ResponseBody
    @GetMapping(value = "/users/{id}/notes")
    public List<Note> findNotesForUser(@PathVariable int id)
    {
        return noteRepository.findAllByUserId(id);
    }

    @ResponseBody
    @DeleteMapping("users/{id}/notes")
    public void deleteNoteByUserId(@PathVariable int id)
    {
        noteRepository.deleteByUserId(id);
    }
}
